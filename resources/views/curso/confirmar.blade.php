@extends('layouts.main')

@section('content')
    <h1>Eliminando registro</h1>
    <div>
        ¿Estas seguro de que quieres borrar el registro?
    </div>
    <div class="tarjeta">
        <ul>
            <li>id:{{ $curso->id }}</li>
            <li>Nombre:{{ $curso->nombre }}</li>
            <li>Duracion:{{ $curso -> duracion }}</li>
            <li>Fecha de Comienzo:{{ $curso->fechacomienzo }}</li>
            <li>Observaciones:{{ $curso->observaciones }}</li>
        </ul>
        <form action="{{ route('curso.destroy', $curso) }}" method="post" class="form-inline">
            @csrf
            @method('delete')
            <button type="submit" class="boton">Borrar</button>
        </form>
    </div>
@endsection

