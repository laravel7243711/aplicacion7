<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //  $cursos=Curso::paginate(3);
        $cursos=Curso::all();
        return view('curso.index',compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('curso.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'nombre' => 'required',
        //     'duracion' => 'required',
        // ], [
        //     'required' => 'El campo :attribute es obligatorio',
        // ]);
        $curso=Curso::create($request->all());
        return redirect()
        ->route('curso.show', $curso)
        ->with('mensaje', 'Curso creado');
        
        
    }

    /**
     * Display the specified resource.
     */
    public function show(Curso $curso)
    {
        return view('curso.show',compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Curso $curso)
    {
        return view('curso.edit',compact('curso'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Curso $curso)
    {
        $request->validate([
            'nombre' => 'required',
            'duracion' => 'required',
        ], [
            'required' => 'El campo :attribute es obligatorio',
        ]);
        $curso->update($request->all());
        return redirect()
        ->route('curso.show', $curso)
        ->with('mensaje', 'Curso actualizado');
       
        
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Curso $curso)
    {
        $curso->delete();
        return redirect()
        ->route('curso.index')
        ->with('mensaje', 'Curso eliminado');
    }
    public function confirmar(Curso $curso){
        return view('curso.confirmar',compact('curso'));
    }
}
