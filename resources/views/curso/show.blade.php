@extends('layouts.main')

@section('content')
    <div clas="listado">
    
                  
            
        
        <div class="tarjeta">
        <ul>
          
            <li>Id:{{ $curso->id }}</li>
            <li>Nombre:{{ $curso->nombre }}</li>
            <li>Duración:{{ $curso->duracion }}</li>
            <li>Fecha de Comienzo:{{ $curso->fechacomienzo }}</li>
            <li>Observaciones:{{ $curso->observaciones }}</li>
        </ul>

        <div class="botones">
            <a href={{ route('curso.edit', $curso) }} class="boton">Actualizar</a>
            <form action="{{ route('curso.destroy', $curso) }}" method="post" id="eliminar" class="form-inline">
                @csrf
                @method('delete')
                <button type="submit" class="boton">Borrar</button>
            </form>
                <button type="submit" class="boton">Eliminar otra forma</button>
           
        </div>
    </div>
@endsection
