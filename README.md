# Objetivos

# Diseño de la base de datos

# Conversion al modelo relacional

# Instalar aplicacion base de laravel

~~~php
laravel new aplicacion7
~~~

# Instalar componentes

Instalar sass y bootstrap

~~~php

~~~

Instalar componente de idiomas (colocar los mensajes de error en castellano)

~~~php

~~~

# Configurar aplicacion

En el archivo .env colocamos idioma y nombre de la aplicacion


# Creamos controladores, modelos y migraciones

Creamos los controladores, modelos, migraciones, seeder y factory de todas las tablas.

~~~php
php artisan make:model Alumno -mfscr
~~~

# Terminamos los modelos

En los modelos colocamos 
- propiedad protected para el nombre de la tabla
- propiedad protected para indicar los campos asignacion masiva (fillable)
~~~php
    // nombre de la tabla
    protected $table = 'alumnos';

    // campos de asignacion masiva
    protected $fillable = [
        'nombre', 'apellidos', 'fechanacimiento', 'email', 'foto'
    ];
~~~
- propiedad estatica labels, para colocar las etiquetas de los campos personalizadas
- metodo getAttributeLabel
- metodo getFields
~~~php
    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'ID',
        'nombre' => 'Nombre',
        'apellidos' => 'Apellidos',
        'fechanacimiento' => 'Fecha de Nacimiento',
        'email' => 'Email',
        'fotos' => 'Fotos'
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
~~~

En los modelos añadimos las relaciones
~~~php
    // voy a crear las relaciones de alumno

    public function perteneces(): HasMany
    {
        return $this->hasMany(Pertenece::class);
    }
    public function presentas(): HasMany
    {
        return $this->hasMany(Presenta::class);
    }
~~~