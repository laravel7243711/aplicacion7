@extends('layouts.main')

@section('content')
    <div class="listado">
        @foreach ($cursos as $curso)
            <div class="tarjeta">
                <ul>
                    <li>Id:{{ $curso->id }}</li>
                    <li>Nombre:{{ $curso->nombre }}</li>
                    <li>Duracion:{{ $curso->duracion }}</li>
                    <li>Fecha de Comienzo:{{ $curso->fechacomienzo }}</li>
                    <li>Observaciones:{{ $curso->observaciones }}</li>

                </ul>
                <div class="botones">
                    <a href="{{ route('curso.show', $curso) }}" class="boton">Ver</a>
                    <a href="{{ route('curso.edit', $curso) }}" class="boton">Actualizar</a>
                    <form action="{{ route('curso.destroy', $curso) }}" method="post" id="eliminar" class="form-inline">
                        @csrf
                        @method('delete')
                        <button type="submit" class="boton">Eliminar</button>
                    </form>
                    <a href="{{ route('curso.confirmar', $curso) }}" class="boton">Eliminar de otra forma</a>
                </div>

            </div>
        @endforeach

    </div>
@endsection
