<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Pertenece extends Model
{

    use HasFactory;
    //nombre de la tabla
    protected $table = 'perteneces';
    // campos de la tabla
    protected $fillable =
    ['alumno_id', 'curso_id'];

    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'Id',
        'alumno_id' => 'Id del Alumno ',
        'curso_id' => 'Id del Curso ',
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table());
    }
    // vamos a crear las relacciones
    // relacion uno a muchos con cursos
    public function alumno():BelongsTo
    {
        return $this->belongsTo(Alumno::class);
    }
    public function curso():BelongsTo
    {
        return $this->belongsTo(Curso::class);
    }
}
