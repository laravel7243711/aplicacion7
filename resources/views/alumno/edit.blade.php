@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('alumno.update', $alumno) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div>
                <label>Nombre</label>
                <input type="text" name="nombre" value="{{ $alumno->nombre }} required">
            </div>
            <div>
                <label>Apellidos</label>
                <input type="text" name="apellidos" value="{{ $alumno->apellidos }} required">
            </div>
            <div>
                <label>Email</label>
                <input type="email" name="email" value="{{ $alumno->email }}  required">
            </div>
            <div>
                <label>Fecha Nacimiento</label>
                <input type="date" name="fechanacimiento required">
            </div>
            <div>
                <label>Foto</label>
                <img src="{{ asset('storage/' . $alumno->foto) }}"id="preview">
                <input type="file" name="foto" value="{{ $alumno->foto }}"id="fichero">
            </div>
            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endsection

@section('css')
    <style>
        .#preview {
            max-width: 100%;
            width: 200px;
        }
    </style>
@endsection