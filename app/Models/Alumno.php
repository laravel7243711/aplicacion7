<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Alumno extends Model
{

    use HasFactory;
    //nombre de la tabla
    protected $table = 'alumnos';
    // campos de la tabla
    protected $fillable =
    ['nombre', 'apellidos', 'fechanacimiento', 'email', 'foto'];

    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'Id',
        'nombre' => 'Nombre',
        'apellidos' => 'Apellidos',
        'fechanacimiento' => 'Fecha de Nacimiento',
        'email' => 'Email',
        'fotos' => 'Fotos',
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table());
    }
    // vamos a crear las relacciones
    // relacion uno a muchos con cursos
    public function presentas() :HasMany
    {
        return $this->hasMany(Presenta::class);
    }
    public function perteneces():HasMany
    {
        return $this->hasMany(Pertenece::class);
    }
}
