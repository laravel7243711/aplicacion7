@extends('layouts.main')

@section('content')
<div class="tarjeta">
<form action="{{ route('curso.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre">
    </div>
    <div>
        <label for="duracion">Duración</label>
        <input type="number" name="duracion" id="duracion">
    </div>
    <div>
        <label for="fechacomienzo">Fecha de Comienzo</label>
        <input type="date" name="fechacomienzo" id="fechacomienzo">
    </div>
    <div>
        <label for="observaciones">Observaciones</label>
        <input type="text" name="observaciones" id="observaciones">
    </div>
   
    <div>
        <button type="submit" class="boton">Enviar</button>
    </div>
</form>

</div>
@endsection
