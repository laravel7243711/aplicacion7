<?php
use App\Http\Controllers\PerteneceController;
use App\Http\Controllers\PracticaController;
use App\Http\Controllers\AlumnoController;
use App\Http\Controllers\CursoController;
use App\Http\Controllers\PresentaController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
})->name('inicio');
// enrutamiento para controlador alumno
Route::resource('alumno', AlumnoController::class);

// entutamiento para controlador curso
Route::resource('curso', CursoController::class);
// enrutamiento para controlador practica
Route::resource('practica', PracticaController::class);
// enrutamiento para controlador presenta
Route::resource('presenta', PresentaController::class);
// enrutamiento para perteneces
Route::resource('pertenece', PerteneceController::class);

// enrutamiento adicional para practicas
Route::controller(PracticaController::class)->group(function () {
    Route::get('/practica/confirmar/{practica}',
     'confirmar')->name('practica.confirmar');
});
// enrutamiento adicional para cursos
Route::controller(CursoController::class)->group(function () {
    Route::get('/curso/confirmar/{curso}',
     'confirmar')->name('curso.confirmar');
});

// entrutamiento adicional para alumnos
Route::controller(AlumnoController::class)->group(function () {
    Route::get('/alumno/confirmar/{alumno}',
     'confirmar')->name('alumno.confirmar');

});
// entrutamiento adicional para pertenece
Route::controller(PerteneceController::class)->group(function () {
    Route::get('/pertenece/confirmar/{pertenece}',
     'confirmar')->name('pertenece.confirmar');
});