@extends('layouts.main')

@section('content')
<div class="tarjeta">
    <form action="{{ route('curso.update', $curso) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div>
            <label>Id</label>
            <input type="number" name="id" required value="{{ $curso->id }} ">
        </div>
        <div>
            <label>Nombre</label>
            <input type="text" name="nombre" required value="{{ $curso->nombre }} ">
        </div>
        <div>
            <label>Duración</label>
            <input type="number" name="duracion" value="{{ $curso->duracion }}  required">
        </div>
        <div>
            <label>Fecha Comienzo</label>
            <input type="date" name="fechacomienzo" value="{{ $curso->fechacomienzo }} required">
        </div>
        <div>
            <label>Observaciones</label>
            <input type="text" name="observaciones" value="{{ $curso->observaciones }} required">
        </div>
        <div>
            <button type="submit" class="boton">Enviar</button>
        </div>

    </form>

</div>  
@endsection
