@extends('layouts.main')

@section('content')
<div class="tarjeta">
    <form action="{{ route('presenta.store') }}" method="post" enctype="multipart/form-data">
        @csrf

        <div>
            <label for="alumno.id">Alumno Id</label>
            <select name="alumno.id">
                @foreach ($alumnos as $alumno)
                    <option value="{{ $alumno->id }}">{{ $alumno->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="practica.id">Practica Id</label>
            <select name="practica.id">
                @foreach ($practicas as $practica)
                    <option value="{{ $practica->id }}">{{ $practica->titulo }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="nota">Nota</label>
            <input type="number" name="nota" id="nota">
        </div>

        <div>
            <button type="submit">Enviar</button>
        </div>
    </form>
    </div>
@endsection
