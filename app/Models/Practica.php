<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Schema;

class Practica extends Model
{

    use HasFactory;
    //nombre de la tabla
    protected $table = 'practicas';
    // campos de la tabla
    protected $fillable =
    ['titulo','fichero','curso_id'];

    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'Id',
        'titulo' => 'Título',
        'fichero' => 'Fichero de la practica',
        'curso_id' => 'Curso Id',
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table());
    }
    // vamos a crear las relacciones
    // relacion uno a muchos con cursos
    public function presentas() :HasMany
    {
        return $this->hasMany(Presenta::class);
    }
    public function curso(): BelongsTo
    {
        return $this->belongsTo(Curso::class);
    }
}
