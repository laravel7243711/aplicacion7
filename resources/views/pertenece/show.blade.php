@extends('layouts.main')

@section('content')
    <div class="listado">
        <ul>
            <li>Id:{{ $pertenece->id }}</li>
            <li>Alumno Id:{{ $pertenece->alumno_id }}-{{ $pertenece->alumno->nombre }}</li>
            <li>Curso Id:{{ $pertenece->curso->id }}-{{$pertenece->curso->nombre}}</li>
        </ul>
        <div class="botones">
            <a href="{{ route('pertenece.show', $pertenece) }}" class="boton">Ver</a>
            <a href="{{ route('pertenece.edit', $pertenece) }}" class="boton">Actualizar</a>
            <form action="{{ route('pertenece.destroy', $pertenece) }}" method="post" id="eliminar" class="form-inline">
                @csrf
                @method('delete')
                <button type="submit" class="boton">Eliminar</button>
            </form>
            <a href="{{ route('pertenece.confirmar', $pertenece) }}" class="boton">Eliminar de otra forma</a>
        </div>
    </div>
@endsection
