<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $alumnos=Alumno::paginate(6);// me devuelve los alumnos de 6 en 6
        // tenemos una coleccion de objetos de tipo alumno
        return view('alumno.index',compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('alumno.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());

        // $request->validate([
        //     'nombre' => 'required',
        //     'apellidos' => 'required',
        //     'email' => 'required',
        //     'telefono' => 'required',
        // ], [
        //     'required' => 'El campo :attribute es obligatorio',
        //     'email' => 'El campo :attribute debe ser un correo electronico',
        // ]);
        // obtengo la foto subida
        $foto=$request->file('foto');
// la almacino en el disco publico
        $fichero= $foto->store('fotos','public');
        // creo el elemento en la base de datos
        $alumno=new Alumno();
        
        $alumno->fill($request->all());
        $alumno->foto=$fichero;
        $alumno->save();
        // lo redireciono al show
        return redirect()
        ->route('alumno.show', $alumno);
        // ->with('mensaje', 'Alumno creado');

        
        
    }

    /**
     * Display the specified resource.
     */
    public function show(Alumno $alumno)
    {
        return view('alumno.show',compact('alumno'));
        // return $alumno;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Alumno $alumno)
    {
        // mandar los datos al formulario para que los pueda editar el usuario
        return view('alumno.edit',compact('alumno'));
    }

    /**
     * Update the specified resource in storage.
     */
    // recibe un alumno(datos nuevos y datos antiguos)
    public function update(Request $request, Alumno $alumno)
    {
        // $request->validate([
        //     'nombre' => 'required',
        //     'apellidos' => 'required',
        //     'email' => 'required',
        // ], [
        //     'required' => 'El campo :attribute es obligatorio',
        //     'email' => 'El campo :attribute debe ser un correo electronico',
        // ]);

        // compruebo con un if
        if ($request->has('foto')) {
            $foto=$request->file('foto');
            $fichero= $foto->store('fotos','public');
            // con esto eliminamos la foto anterior
            Storage::disk('public')->delete($alumno->foto);
            $alumno->fill($request->all());
            $alumno->foto=$fichero;
            
        }else{
            $alumno->fill($request->all());
        }

        $alumno->save();

        return redirect()
        ->route('alumno.show', $alumno)
        ->with('mensaje', 'Alumno actualizado');
    }

    /**
     * Remove the specified resource from storage.
     * me llega un alumno y debo eliminarlo
     */
    public function destroy(Alumno $alumno)
    {
        // elmininamos la foto
        Storage::disk('public')->delete($alumno->foto);
        // elimina el registro
        $alumno->delete();
        
        return redirect()
        ->route('alumno.index')
        ->with('mensaje', 'Alumno eliminado');
    }
    public function confirmar(Alumno $alumno){
        return view('alumno.confirmar',compact('alumno'));
    }
}
