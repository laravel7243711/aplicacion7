<?php

namespace Database\Seeders;

use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PracticaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
       // Practica::factory(20)->create();
        for($c=0;$c<10;$c++){
            $curso=Curso::factory()->create();
            $totalPracticas=rand(1,10);
            for($numeroPracticas=0;$numeroPracticas<$totalPracticas;$numeroPracticas++)
                           
                Practica::factory()
                ->for($curso)
                ->create();
               
        }
    }
}
