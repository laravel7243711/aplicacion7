<?php

namespace App\Http\Controllers;

use App\Models\Presenta;
use Illuminate\Http\Request;
use App\Models\Alumno;
use App\Models\Practica;

class PresentaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $presentas=Presenta::all();
        return view('presenta.index',compact('presentas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $alumnos=Alumno::all();
        $practicas=Practica::all();
        return view ('presenta.create', compact( 'alumnos','practicas'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Presenta $request)
    {
        //
        $presenta=Presenta::create($request->all());
        return redirect()
        ->route('presenta.show', $presenta);
        // ->with('mensaje', 'Presenta creada');
    }

    /**
     * Display the specified resource.
     */
    public function show(Presenta $presentum)
    {
        //
        return view('presenta.show',compact('presenta'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Presenta $resenta )
    {
        //
        $alumnos=Alumno::all();
        $practicas=Practica::all();
        return view('presenta.edit',compact('presenta','alumnos','practicas'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update( Request $request, Presenta $presenta)
    {
        $presenta->update($request->all());
        return redirect()
        ->route('presenta.show', $presenta);
       // ->with('mensaje', 'Presenta actualizada');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Presenta $presentum)
    {
        //
        $presentum->delete();
        return redirect()
        ->route('presenta.index');
        // ->with('mensaje', 'Presenta eliminada');
    }
    public function confirmar(Presenta $presenta)   
    {
        return view('presenta.confirmar',compact('presenta'));
    }
}
