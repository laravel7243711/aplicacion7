@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('presenta.update', $presenta) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')


            <div>
                <label>Alumno Id</label>
                <select name="alumno.id">
                    @foreach ($alumnos as $alumno)
                        <option value="{{ $alumno->id }}" {{ $presenta->alumno_id == $alumno->id ? 'selected' : '' }}>
                            {{ $alumno->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label>Id Practica</label>
                <select name="practica.id">
                    @foreach ($practicas as $practica)
                        <option value="{{ $practica->id }}" {{ $presenta->practica_id == $practica->id ? 'selected' : '' }}>
                            {{ $practica->titulo }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label>Nota</label>
                <input type="number" name="nota" value="{{ $presenta->nota }} required">

                <div>
                    <button type="submit">Enviar</button>
                </div>

        </form>
    </div>
@endsection
