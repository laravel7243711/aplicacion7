<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Schema;

class Presenta extends Model
{

    use HasFactory;
    //nombre de la tabla
    protected $table = 'presentas';
    // campos de la tabla
    protected $fillable =
    ['alumno_id', 'practica_id','nota'];

    // creamos un atributo estatico con los labels
    public static $labels = [
        'id' => 'Id',
        'alumno_id' => 'Alumno Id',
        'practica_id' => 'Practica Id',
        'nota' => 'Nota',
    ];

    // metodo para devolver el label de un campo
    public function getAttributeLabel($key)
    {
        return self::$labels[$key] ?? $key;
    }

    public function getFields()
    {
        return Schema::getColumnListing($this->table());
    }
    // vamos a crear las relacciones
    // relacion uno a muchos con cursos
    public function alumno() :BelongsTo
    {
        return $this->belongsTo(Alumno::class);
    }
    public function practica():BelongsTo
    {
        return $this->belongsTo(Pertenece::class);
    }
}
