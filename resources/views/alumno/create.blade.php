@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('alumno.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div>
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre">
            </div>
            <div>
                <label for="apellidos">Apellidos</label>
                <input type="text" name="apellidos" id="apellidos">
            </div>
            <div>
                <label for="fechanacimiento">Fecha de Nacimiento</label>
                <input type="date" name="fechanacimiento" id="fechanacimiento">
            </div>
            <div>
                <label for="email">Email</label>
                <input type="email" name="email" id="email">
            </div>
            <div>
                <label for="foto">Foto</label>
                <img id="preview">
                <input type="file" name="foto" id="fichero">
            </div>
            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endsection
@section('css')
    <style>
        .#preview {
            max-width: 100%;
            width: 200px;
        }
    </style>
@endsection