@extends('layouts.main')

@section('content')
    <div class="listado">
        @foreach ($presentas as $presenta)
            <div class="tarjeta">
                <ul>
                    <li>Id:{{ $presenta->id }}</li>
                    <li>Alumno Id:{{ $presenta->alumno_id }}</li>
                    <li>Practica Id:{{ $presenta->practica_id }}</li>
                    <li>Nota:{{ $presenta->nota }}</li>
                </ul>
                <div class="botones">
                    <a href="{{ route('presenta.show', $presenta) }}" class="boton">Ver</a>
                    <a href="{{ route('presenta.edit', $presenta) }}" class="boton">Actualizar</a>
                    <form action="{{ route('presenta.destroy', $presenta) }}" method="post" iliminar class="form-inline">
                        @csrf
                        @method('delete')
                        <button type="submit">Eliminar</button>
                    </form>
                    
                </div>

            </div>
        @endforeach
    </div>
@endsection
