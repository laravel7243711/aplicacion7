<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Practica;
use App\Models\Presenta;
use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        // opcion 1 sin utilizar los seeder

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // podemos llamar a los factories desde aquí directamente sin necesidad de seeder
        // Alumno::factory(10)->create();
        // Curso::factory(10)->create();
        
        // opcion 2 
        // utilizando los seeder
        $this->call([
             AlumnoSeeder::class,
            CursoSeeder::class,
            PracticaSeeder::class,
             PerteneceSeeder::class,
            PresentaSeeder::class
        ]);
        
        
    }
}
