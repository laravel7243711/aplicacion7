@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('pertenece.store') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div>
                <label for="alumno_id">Alumno Id</label>
                <select name="alumno_id">
                    @foreach ($alumnos as $alumno)
                        <option value="{{ $alumno->id }}">{{ $alumno->nombre }}</option>
                    @endforeach
                </select>

            </div>
            <div>
                <label for="curso_id">Curso Id</label>
                <select name="curso_id">
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}">{{ $curso->nombre }}</option>
                    @endforeach
                </select>

            </div>
            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endSection
