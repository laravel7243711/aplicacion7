@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('pertenece.update', $pertenece) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div>
                <label>Curso</label>
                <select name="curso_id">
                    @foreach ($cursos as $curso)
                        <option value="{{ $curso->id }}" {{ $pertenece->curso_id == $curso->id ? 'selected' : '' }}>
                            {{ $curso->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label>Alumno</label>
                <select name="alumno_id">
                    @foreach ($alumnos as $alumno)
                        <option value="{{ $alumno->id }}" {{ $pertenece->alumno_id == $alumno->id ? 'selected' : '' }}>
                            {{ $alumno->nombre }}</option>
                    @endforeach
                </select>
            </div>

            <div>
                <button type="submit" class="boton">Enviar</button>
            </div>
        </form>
    </div>
@endSection
