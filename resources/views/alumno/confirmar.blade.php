@extends('layouts.main')

@section('content')
    <h1>Eliminando registro</h1>
    <div>
        ¿Estas seguro de que quieres borrar el registro?
    </div>
    <div class="tarjeta">

        <ul>
            <li>Id:{{ $alumno->id }}</li>
            <li>Nombre:{{ $alumno->nombre }}</li>
            <li>Apellidos:{{ $alumno->apellidos }}</li>
            <li>Fecha de Nacimiento:{{ $alumno->fechanacimiento }}</li>
            <li>Email:{{ $alumno->email }}</li>
            <li>Foto:{{ $alumno->foto }}</li>

        </ul>


        <form action="{{ route('alumno.destroy', $alumno) }}" method="post" id="eliminar">
            @csrf
            @method('delete')
            <button type="submit">Eliminar</button>
        </form>



    </div>
@endsection
