@extends('layouts.main')

@section('content')
    <div class="listado">
        @foreach ($alumnos as $alumno)
            <div class="tarjeta">
                <ul>
                    <li>Id:{{ $alumno->id }}</li>
                    <li>Nombre:{{ $alumno->nombre }}</li>
                    <li>Apellidos:{{ $alumno->apellidos }}</li>
                    <li>Fecha de Nacimiento:{{ $alumno->fechanacimiento }}</li>
                    <li>Email:{{ $alumno->email }}</li>
                    <li>Foto:{{ $alumno->foto }}</li>
                </ul>
                <div class="botones">
                    <a href="{{ route('alumno.show', $alumno) }}"class="boton">Ver</a>
                    <a href="{{ route('alumno.edit', $alumno) }}" class="boton">Actualizar</a>
                    <form action="{{ route('alumno.destroy', $alumno) }}" method="post" id="eliminar" class="form-inline">
                        @csrf
                        @method('delete')
                        <button type="submit" class="boton">Eliminar</button>
                    </form>
                    <a href="{{ route('alumno.confirmar', $alumno) }}" class="boton">Eliminar de otra forma</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
