@extends('layouts.main')

@section('content')
<div class="listado">
<ul>
    <li>Id:{{ $presenta->id }}</li>
    <li>Alumno Id:{{ $presenta->alumno->id }}</li>
    <li>Practica Id:{{ $presenta->practica->id }}</li>
    <li>Nota:{{ $presenta->nota }}</li>
</ul>
    <div class="botones">
        <a href={{ route('presenta.show', $presenta) }} class="boton">Ver</a>
        <a href={{ route('presenta.edit', $presenta)}} class="boton">Actualizar</a>
        <form action="{{ route('presenta.destroy', $presenta) }}" method="post" id="eliminar" class="form-inline">
            @csrf
            @method('delete')
            <button type="submit">Eliminar</button>
        </form>
        <a href="{{ route('presenta.confirmar', $presenta) }}" class="boton">Eliminar de otra forma</a>
    </div>
</div>
@endsection

