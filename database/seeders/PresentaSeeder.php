<?php

namespace Database\Seeders;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use App\Models\Practica;
use App\Models\Presenta;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PresentaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($numero = 0; $numero < 10; $numero++) {
            // creo un alumno
            $alumno = Alumno::factory()->create();

            // creo un curso
            $curso = Curso::factory()->create();

            // creo una practica en el curso
            $practica = Practica::factory()
                ->for($curso)
                ->create();


            // indico que el alumno esta realizando la practica
            Presenta::factory()
                ->for($alumno)
                ->for($practica)
                ->create();

            // indico que el alumno esta realizando el curso
            Pertenece::factory()
                ->for($alumno)
                ->for($curso)
                ->create();
        }
    }
}
